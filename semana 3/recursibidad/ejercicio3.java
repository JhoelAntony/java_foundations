import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int num = sc.nextInt();
        int mcm = calcularMCM(num);
        System.out.println("Los múltiplos de " + num + " son: ");
        for (int i = 1; i <= 10; i++) {
            System.out.print(mcm * i + " ");
        }
    }

    public static int calcularMCM(int num) {
        int mcm = num;
        for (int i = 1; i <= num; i++) {
            if (mcm % i != 0) {
                mcm += num;
                i = 0;
            }
        }
        return mcm;
    }
}