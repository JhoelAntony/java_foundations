import java.util.Scanner;
public class ejercicio2{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int num = sc.nextInt();
        int mcd = calcularMCD(num);
        System.out.println("El máximo común divisor de " + num + " es " + mcd);
        System.out.print("Los divisores de " + num + " son: ");
        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                System.out.print(i + " ");
            }
        }
    }

    public static int calcularMCD(int num) {
        int mcd = num;
        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                mcd = i;
            }
        }
        return mcd;
    }
}