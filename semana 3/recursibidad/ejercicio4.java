import java.math.BigInteger;
import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de elementos en la lista: ");
        int n = sc.nextInt();
        BigInteger factorial = calcularFactorial(n);
        System.out.println("El número de permutaciones de " + n + " elementos es " + factorial);
    }

    public static BigInteger calcularFactorial(int n) {
        BigInteger factorial = BigInteger.ONE;
        for (int i = 2; i <= n; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }
}