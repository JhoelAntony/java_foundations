import java.util.Scanner;
public class  ejercicio7{
    public static void main(String[] args) {
        int n, suma = 0, num = 2, cont = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        n = sc.nextInt();
        do {
            suma += num;
            num += 2;
            cont++;
        } while (cont < n);
        System.out.println("La suma de los " + n + " números pares es: " + suma);
    }
}