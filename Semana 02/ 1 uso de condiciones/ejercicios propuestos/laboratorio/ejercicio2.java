import java.util.Scanner;
public class  ejercicio2{
    public static void main(String[] args) {
        int n, suma = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        n = sc.nextInt();
        for (int i = 1; i <= n * 2; i += 2) {
            suma += i;
        }
        System.out.println("La suma de los " + n + " números impares es: " + suma);
    }
}