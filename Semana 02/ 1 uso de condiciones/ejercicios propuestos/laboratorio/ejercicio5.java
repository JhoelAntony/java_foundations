import java.util.Scanner;

public class ejercicio5 {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    System.out.print("Ingrese un numero: ");
    int n = input.nextInt();
    System.out.print("Números primos del 2 al " + n + ": ");
    for (int i = 2; i <= n; i++) {
      boolean isPrime = true;
      for (int j = 2; j <= Math.sqrt(i); j++) {
        if (i % j == 0) {
          isPrime = false;
          break;
        }
      }
      if (isPrime) {
        System.out.print(i + " ");
      }
    }
  }
}