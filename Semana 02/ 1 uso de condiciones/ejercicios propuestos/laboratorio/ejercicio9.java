import java.util.Scanner;

public class ejercicio9 {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    System.out.print("Ingrese el numero: ");
    int num = input.nextInt();
    int sum = 0;
    int i = 1;
    do {
      if (num % i == 0) {
        sum += i;
      }
      i++;
    } while (i <= num / 2);
    if (sum == num) {
      System.out.println(num + " Es un numero perfecto.");
    } else {
      System.out.println(num + " No es un numero perfecto.");
    }
  }
}