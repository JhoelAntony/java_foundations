import java.util.Scanner;

public class ejercicio1 {
    public static void main(String[] args) {
        int n1 = 0, n2 = 1, n3 = 0, limite;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un numero: ");
        limite = sc.nextInt();

        System.out.print(n1 + " " + n2);

        while (n3 < limite) {
            n3 = n1 + n2;
            System.out.print(" " + n3);
            n1 = n2;
            n2 = n3;
        }
    }
}